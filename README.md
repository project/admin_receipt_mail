# Drupal commerce admin Recipt Mail

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## Introduction

Drupal commerce admin can able to take receipt mail on every order.
by using module template you can able to customize your mail template.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/admin_receipt_mail).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/admin_receipt_mail).


## Requirements

This module requires the following modules:

- [commerce](https://www.drupal.org/project/commerce)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Path: admin/config/system/site-information
   All operations could be done from mentioned path.


## Maintainers

Supporting by:

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
